const inputPassword = document.querySelector('.password');
const iconPassword = document.querySelector('.fa-eye-slash');
const inputConfirmPassword = document.querySelector('.confirm-password');
const iconPasswordConfirm = document.querySelector('.icon-password-confirm');
const submit = document.querySelector('.btn');
const phrase = document.createElement('p');

function showPassword(input, icon) {
    phrase.innerHTML = "";
    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
        icon.classList.remove('hide');
    } else {
        icon.classList.add('hide');
        input.setAttribute('type', 'password');
    }
};

inputPassword.addEventListener('click', function(e) {
    showPassword(inputPassword, iconPassword);
});
inputConfirmPassword.addEventListener('click', function(e) {
    showPassword(inputConfirmPassword, iconPasswordConfirm)
});

submit.addEventListener('click', function(e) {
    if (inputPassword.value === inputConfirmPassword.value && inputConfirmPassword.value !== "") {
        alert('You are welcome');

    } else {
        phrase.textContent = ('Нужно ввести одинаковые значения');
        phrase.style.color = '#ff0000';
        inputConfirmPassword.after(phrase);
    }
})